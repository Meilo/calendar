# -*- coding:utf-8 -*-

from flask import *
import requests
from datetime import datetime
from dateutil.relativedelta import *
import pymssql
from PyBambooHR import PyBambooHR

app = Flask(__name__)

bamboo = PyBambooHR(subdomain='#####', api_key='###################################')
employees = bamboo.get_employee_directory()
whos_outs = bamboo.get_whos_out(end_date="#######")

@app.route('/directory')
def directory():
	result = []
	for employee in employees:
		if employee['department'] == "Dev Mail":
			if employee['workEmail']:
				result.append(employee)
	return json.dumps(result)

@app.route('/whos_out')
def whosout():
	result = []
	for employee in employees:
		if employee['department'] == "Dev Mail":
			for wo in whos_outs:
				if str(employee['id']) == str(wo['employeeId']):
					result.append(wo)
	return json.dumps(result)

@app.route('/equipe_dev_mail', methods=['GET', 'POST'])
def data_equipe_dev_mail():
	dsn = r'###########'
	user = r'####################'
	password = r'###########'
	database = r'########'
	
	conn = pymssql.connect(host=dsn, user=user, password=password, database=database, as_dict=True)
	
	req_deux = conn.cursor()
	req_deux.execute("SELECT nomEquipe,re.id_equipe,re.loginResponsable, email, nomPoste, ref.dev  FROM [Suivi_Routage].[dbo].[refEquipes] re  left join [Suivi_Routage].[dbo].[repartitionEquipesFifo] ref on re.id_equipe = ref.id_equipe  where re.id_equipe > 0  order by re.id_equipe")
	rows_deux = req_deux.fetchall()

	return json.dumps(rows_deux)

@app.route('/jours_ferier', methods=['GET', 'POST'])
def data_jours_ferier():
	dsn = r'###########'
	user = r'####################'
	password = r'###########'
	database = r'########'

	conn = pymssql.connect(host=dsn, user=user, password=password, database=database, as_dict=True)

	req_deux = conn.cursor()
	req_deux.execute("SELECT DescriptionFR, Jour FROM Ref_JoursFeries")
	rows_deux = req_deux.fetchall()

	return json.dumps(rows_deux)

@app.route('/<adresse>', methods=['GET', 'POST'])
def lync(adresse):
	return redirect(adresse)

@app.route('/calendar', methods=['GET', 'POST'])
def calendar():
	result = []
	global today
	today = ""
	if datetime.today().weekday() == 0:
		today = datetime.today()
		end  = today + relativedelta(months=2)
		while today <= end:
			test = today + relativedelta(day=31)
			dictionnaire = {}
			dictionnaire["dateHeader"] = today.strftime('%a %d')
			dictionnaire["date"] = today.strftime("%Y-%m-%d")
			dictionnaire["month"] = today.strftime("%B")
			dictionnaire["day"] = today.weekday()
			dictionnaire["currentDay"] = datetime.today().weekday()
			dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
			result.append(dictionnaire)
			today = today + relativedelta(days=1)
		return json.dumps(result)
	elif datetime.today().weekday() == 1:
		today = datetime.today() - relativedelta(days=1)
		end  = today + relativedelta(months=2)
		while today <= end:
			test = today + relativedelta(day=31)
			dictionnaire = {}
			dictionnaire["dateHeader"] = today.strftime('%a %d')
			dictionnaire["date"] = today.strftime("%Y-%m-%d")
			dictionnaire["month"] = today.strftime("%B")
			dictionnaire["day"] = today.weekday()
			dictionnaire["currentDay"] = datetime.today().weekday()
			dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
			result.append(dictionnaire)
			today = today + relativedelta(days=1)
		return json.dumps(result)
	elif datetime.today().weekday() == 2:
		today = datetime.today() - relativedelta(days=2)
		end  = today + relativedelta(months=2)
		while today <= end:
			test = today + relativedelta(day=31)
			dictionnaire = {}
			dictionnaire["dateHeader"] = today.strftime('%a %d')
			dictionnaire["date"] = today.strftime("%Y-%m-%d")
			dictionnaire["month"] = today.strftime("%B")
			dictionnaire["day"] = today.weekday()
			dictionnaire["currentDay"] = datetime.today().weekday()
			dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
			result.append(dictionnaire)
			today = today + relativedelta(days=1)
		return json.dumps(result)
	elif datetime.today().weekday() == 3:
		today = datetime.today() - relativedelta(days=3)
		end  = today + relativedelta(months=2)
		while today <= end:
			test = today + relativedelta(day=31)
			dictionnaire = {}
			dictionnaire["dateHeader"] = today.strftime('%a %d')
			dictionnaire["date"] = today.strftime("%Y-%m-%d")
			dictionnaire["month"] = today.strftime("%B")
			dictionnaire["day"] = today.weekday()
			dictionnaire["currentDay"] = datetime.today().weekday()
			dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
			result.append(dictionnaire)
			today = today + relativedelta(days=1)
		return json.dumps(result)
	elif datetime.today().weekday() == 4:
		today = datetime.today() - relativedelta(days=4)
		end  = today + relativedelta(months=2)
		while today <= end:
			test = today + relativedelta(day=31)
			dictionnaire = {}
			dictionnaire["dateHeader"] = today.strftime('%a %d')
			dictionnaire["date"] = today.strftime("%Y-%m-%d")
			dictionnaire["month"] = today.strftime("%B")
			dictionnaire["day"] = today.weekday()
			dictionnaire["currentDay"] = datetime.today().weekday()
			dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
			result.append(dictionnaire)
			today = today + relativedelta(days=1)
		return json.dumps(result)

@app.route("/", methods=['GET', 'POST'])
def index():
	today = datetime.today()
	result = str(today).split(" ")
	req = result[0].split("-")
	res = req[2]+"-"+req[1]+"-"+req[0]
	today2 = datetime.today() + relativedelta(months=2)
	result2 = str(today2).split(" ")
	req2 = result2[0].split("-")
	res2 = req2[2]+"-"+req2[1]+"-"+req2[0]
	return render_template('calendar.html', today=res, today2=res2)

@app.route('/devMail_request/<start>/<end>/<number1>/<number2>', methods=['GET', 'POST'])
def index_request(start, end, number1, number2):
	start_coupe = start.split('-')
	end_coupe = end.split('-')
	result_start = start_coupe[0]+'-'+start_coupe[1]+'-'+start_coupe[2]
	result_end = end_coupe[0]+'-'+end_coupe[1]+'-'+end_coupe[2]
	return render_template('calendar_request.html', start=result_start, end=result_end, nb1=number1, nb2=number2)

@app.route('/calendar_request', methods=['GET', 'POST'])
def calendar_request():
	if request.method == 'POST':
		start = request.form['totaldays1']
		end_1 = request.form['totaldays2']
		start_str = request.form['start']
		end_str = request.form['end']
		if start_str != "" and end_str != "":
			lol = start_str.split('-')
			lol1 = end_str.split('-')
			Letest = lol[0]+'-'+lol[1]+'-'+lol[2]
			Letest1 = lol1[0]+'-'+lol1[1]+'-'+lol1[2]
			if int(start) <= int(end_1):
				global result
				result = []
				today = datetime.today() + relativedelta(days=int(start.decode('cp1252').encode('utf-8')))
				end_test  = datetime.today() + relativedelta(days=int(end_1.decode('cp1252').encode('utf-8')))
				while today <= end_test:
					test = today + relativedelta(day=31)
					dictionnaire = {}
					dictionnaire["dateHeader"] = today.strftime('%a %d')
					dictionnaire["date"] = today.strftime("%Y-%m-%d")
					dictionnaire["month"] = today.strftime("%B")
					dictionnaire["day"] = today.weekday()
					dictionnaire["end_day"] = test.strftime("%Y-%m-%d")
					result.append(dictionnaire)
					today = today + relativedelta(days=1)

				return redirect('/devMail_request/'+Letest+'/'+Letest1+'/'+str(start)+'/'+str(end_1))
			else:
				return redirect('/')
		else:
			return redirect('/')
	return json.dumps(result)


if __name__ == '__main__':
	app.run(debug=True)
