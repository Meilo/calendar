var app = angular.module('PlanningDev', []);

app.config(function($interpolateProvider) { 
      $interpolateProvider.startSymbol('[['); 
      $interpolateProvider.endSymbol(']]');
    });

app.controller('PlanningController', function ($scope, $http) {
	$scope.absent = []
	$scope.employer = []
	$scope.equipes = []
	$scope.monthTotal = []
	$scope.respon = []
	$scope.myVar = true;

	$scope.count = 0;

	$(".flow").css("width",$(document).width()-150);

	$http.get('/directory').success(function(json){
		$scope.employer = json;
		$http.get('/whos_out').success(function(json){
			$scope.absent = json;
			$http.get('/equipe_dev_mail').success(function (response) {
				$.each(response,function(index,dev){
					if(!checkEquipExist(dev.id_equipe))
						$scope.equipes.push({"id_equipe":dev.id_equipe,"department":$scope.employer[0].department,"members":[]});
						dev.nameR = dev.nomEquipe;
						addDevToEquipe(dev);
				}); 
				
			});

		});

	});

	$http.get('/jours_ferier').success(function(json) {
		$scope.jours_ferier = json;
	});

	$http.get('/calendar_request').success(function (response) {
		$scope.calendar = response;
		$.each($scope.calendar,function(index,date){
			if(!checkMonthExist(date.month))
				$scope.monthTotal.push({"nameMonth":date.month,"nbjour":1,"annee":date.date.split("-")[0], "dernier_jour":date.end_day});
			else
				incrementDay(date.month);
		});
		console.log($scope.monthTotal);

		$.each($scope.monthTotal,function(i,m){
			$scope.count = $scope.count + m.nbjour;
		});
		$(".flow table").css("width",$scope.count*35+"px");
	});

	$scope.compartDayAndDay = function (day1, day2)
	{
		var result = false;
		if (day1 == day2)
		{
			result = true;
		}
		return result
	}
	
	function addDevToEquipe(dev)
	{
		$.each($scope.equipes,function(index,equipe){
			if(equipe.id_equipe == dev.id_equipe)
			{
				if(dev.loginResponsable == dev.dev)
					dev.responsable = 1;
				else
					dev.responsable = 0;
				employer = getEmployee(dev.email);
				dev.whosout = getWhosOutEmployee(employer['id']);
				equipe.members.push(dev);
			}
		});
	}
	
	function getWhosOutEmployee(id)
	{
		tabWhosOut = [];
		$.each($scope.absent,function(index,abs){
			if(abs.employeeId == id)
			{
				tabWhosOut.push(abs);
			}
		});
		return tabWhosOut;
	}

	function getEmployee(email)
	{
		var emp = [];
		$.each($scope.employer,function(index,employee){
			if(email.toLowerCase() == employee.workEmail.toLowerCase())
			{
				emp = employee;
				return false;
			}
		});
		return emp;
	}

	function checkEquipExist(id){
		var exists = false;
		$.each($scope.equipes,function(index,equipe){
			if(equipe.id_equipe == id)
				exists = true;
		});
		return exists;
	}

	function incrementDay(month)
	{
		$.each($scope.monthTotal,function(index,monthT){
			if(monthT.nameMonth == month)
				monthT.nbjour++;
		});
	}

	function checkMonthExist(month){
		var exists = false;
		$.each($scope.monthTotal,function(index,monthT){
			if(monthT.nameMonth == month)
				exists = true;
		});
		return exists;
	}

	function convertDate(str){
  		var d = new Date(str);
  		return addZero(d.getFullYear())+"-"+addZero(d.getMonth()+1)+"-"+d.getDate();
	}

	//If we have the number 9, display 09 instead
	function addZero(num){
  		return (num<10?"0":"")+num;
	}


	$scope.verifMailDev = function(mailBamboo, mailFifo){
		var verif = false;
		if (mailFifo != "test@aa.com"){
			if (mailBamboo.toLowerCase() == mailFifo.toLowerCase()){
				verif = true;
			}
		}
		return verif;
	};

	$scope.verifDateWhosOut = function(whos_outs, date){
		var resultat = false;
		var tab = [];
		var test = "";
		$.each(whos_outs,function(index,outs){
			//list des jour deb
			var debut = outs['start'].match(/\d+/g);
			var dateDeb = new Date(debut[0], debut[1], debut[2]);

			//list des jour fin
			var fin = outs['end'].match(/\d+/g);
			var dateFin = new Date(fin[0], fin[1], fin[2]);

			//list des jours de l'année
			var dWhosOut = date.match(/\d+/g);
			var dateWhosOut = new Date(dWhosOut[0], dWhosOut[1], dWhosOut[2]);

			if(dateWhosOut >= dateDeb && dateWhosOut <= dateFin)
				resultat = true;
				
		});
		return resultat;
	};

	$scope.verifDayWeekEnd = function(dayWeekEnd){
		var result = false;
		if(dayWeekEnd == '5' || dayWeekEnd == '6'){
			result = true;
		}
		return result;
	};

	$scope.verifJoursFerier = function(date){
		var result =  false;
		$.each($scope.jours_ferier, function(index, value){
			var les_jours_ferier = convertDate(value.Jour);
			tab_ferier = les_jours_ferier.split("-");
			tab_date = date.split("-");
			if(tab_date[0] == tab_ferier[0] && tab_date[1] == tab_ferier[1] && tab_date[2] == tab_ferier[2])
			{
				result = true;
			}
		});
		return result;
	}


	$scope.verifNbAbsentOfDay = function(n, i){
	var tab = [];
	var result = "";
	$.each(n['members'],function(index, lol){
			$.each(lol['whosout'], function(index, test){

			var debut = test['start'].match(/\d+/g);
			var dateDeb = new Date(debut[0], debut[1], debut[2]);

			//list des jour fin
			var fin = test['end'].match(/\d+/g);
			var dateFin = new Date(fin[0], fin[1], fin[2]);

			//list des jours de l'année
			var dWhosOut = i.match(/\d+/g);
			var dateWhosOut = new Date(dWhosOut[0], dWhosOut[1], dWhosOut[2]);

			if(dateWhosOut >= dateDeb && dateWhosOut <= dateFin)
				tab.push(test['employeeName']);
			});
		});
		var ope = tab.length / n['members'].length;
		var res = ope * 100;
		if(res == 50)
			result = "orange";
		else if(res > 50)
			result = "rouge";
		else if(res < 50)
			result = "vert";
		return result;
	};

});

app.filter('filterDev', function () {
    return function (dev) {  
        return dev.filter(function (d) {
        	var show = true;
        	$.each(dev ,function(i,obj){
        		
        		if(obj.responsable != 1)
        		{
        			if($("#"+obj.id_equipe).is(":checked") == false) {
        				show = false;
        			}
        		}
        	});
        	return show;
        });
    };
});