$(document).ready(function(){

	$(".flow").mousewheel(function(event, delta) {
		this.scrollLeft -= (delta * 30);
		event.preventDefault();
	});

		$('#fin1').datepicker({minDate: 1, dateFormat: "dd-mm-yy", onSelect: function(selectedDate) {
		    var maxDate = $(this).datepicker('getDate');
		    if (maxDate) {maxDate.setDate(maxDate.getDate() - 1);}  
		    days1();
		}});
		$('#fin2').datepicker({minDate: 1, dateFormat: "dd-mm-yy", onSelect: function(selectedDate) {
		    var maxDate = $(this).datepicker('getDate');
		    if (maxDate) {maxDate.setDate(maxDate.getDate() - 1);}  
		    days2();
		}});
    	
    	$('#fin1').change();
    	$('#fin2').change();
      days1();
      days2();

});

function days1(){
  		var a = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime(),
  		b = $("#fin1").datepicker('getDate').getTime(),
  		c = 24*60*60*1000,
  		diffDays = Math.round(Math.abs((a - b)/(c)));
  		$('#totaldays1').val(diffDays);
  	};

  	function days2(){
  		var a = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime(),
  		b = $("#fin2").datepicker('getDate').getTime(),
  		c = 24*60*60*1000,
  		diffDays = Math.round(Math.abs((a - b)/(c)));
  		$('#totaldays2').val(diffDays);
  	};